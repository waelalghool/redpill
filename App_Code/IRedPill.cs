﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;

[DataContract(Name = "TriangleType", Namespace = "http://KnockKnock.readify.net")]
public enum TriangleType
{
    [EnumMember]
    Error = 0,
    [EnumMember]
    Equilateral = 1,
    [EnumMember]
    Isosceles = 2,
    [EnumMember]
    Scalene = 3
}


[ServiceContract(Namespace = "http://KnockKnock.readify.net")]
public interface IRedPill
{
    [OperationContract]
    [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Xml)]
    Guid WhatIsYourToken();

    [OperationContract]
    [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Xml)]
    [FaultContract(typeof(ArgumentOutOfRangeException))]
    long FibonacciNumber(long n);

    [OperationContract]
    [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Xml)]
    TriangleType WhatShapeIsThis(int a, int b, int c);

    [OperationContract]
    [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Xml)]
    [FaultContract(typeof(ArgumentNullException))]
    string ReverseWords(string s);

}

