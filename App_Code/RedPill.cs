﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Diagnostics;
using System.ServiceModel.Channels;
using System.IO;

[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
[ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
public class RedPill : IRedPill
{
    public Guid WhatIsYourToken()
    {
        return new Guid("d7adbc1c-dfa6-4c42-b0fa-ad462ae6cd89");
    }

    public string ReverseWords(string s)
    {
        //--------------------------------------------------------------------------------------------------------
        //  *** UTF8 encoding discussion ***
        //--------------------------------------------------------------------------------------------------------
        //  The 3-bytes "E2 B8 AE", are representing single UTF8 char!
        //  Let's discuss that issue little a bit ............
        //  The "Replacement Character" is an arbitrary symbole that appears like a diamond in UTF8-editors, 
        //  it is a single-byte char with invalid UTF8-encoding where the editor can not identify.
        //  Encoding system like UTF8 knows the invalide char if it is not obying UTF8's encoding-schema.
        //  UTF8 schema designates itself with two specific (bit-pattern)s, the first bit-pattern represents the 
        //  number of bytes that is going to represent None-ASCII-char in the text-buffer, the second bit-pattern is 
        //  the bit-mark "10" at the begining of every-next byte in the UTF8-char.
        //  Example: 
        //      The UTF8-Char "E2 B8 AE" bit representation is "11100010 10111000 10101110"; 
        //      where 1110 in the first byte means that the cahr is represented by 3-bytes an devery byte begins with 10
        //--------------------------------------------------------------------------------------------------------

        //throw in case of empty string
        if (string.IsNullOrEmpty(s))
        {
            throw new ArgumentNullException("The word must have a value, it can not be empty", "s");
        }
        
        //Get char array to make every thing fast
        char[] ca = s.ToCharArray();
        int c = ca.Count();//get chars count
        int l = c - 1;//get latest index
        int m = c / 2;//determine middle index
        for (int x = 0; x < m; x++) //loop to the middle
        {
            //swap using fast approach
            ca[l - x] ^= ca[x];
            ca[x] ^= ca[l - x];
            ca[l - x] ^= ca[x];
        }

        //return the reveresed string
        return new string(ca);
    }

    public long FibonacciNumber(long n)
    {
        //--------------------------------------------------------------------------------------------------------
        //  *** Fibonacci discussion ***
        //--------------------------------------------------------------------------------------------------------
        //  In mathematical terms, the sequence Fn of Fibonacci numbers is defined by the recurrence relation:
        //      F(n)=F(n-1)+F(n-2)
        //  with seed values 
        //      F(0)=0, F(1)=1, F(2)=1
        //  so, F(n) = 0, 1, 1, 2, 3, 5, 8, 13, 21, ... where  n is a 0-based value n=0,1,2,3,4,5,6 ...
        //  Example:
        //      F6=8
        //
        //  For negative index n where n=-1,-2,-3,-4,-5,-6,-7... the recursive relation would be:
        //      F(n-2) = F(n) - F(n-1)
        //      or
        //      F(-n) = power(-1,n+1) * F(n) where n = abs(-n)
        //
        //  The minimum-n to generate maximum-long is n=92, 
        //      F(92)               = 7,540,113,804,746,346,429 
        //      Int64.MaxValue      = 9,223,372,036,854,775,807
        //--------------------------------------------------------------------------------------------------------

        //return seeds
        if (n == 0)
            return 0;

        if (n == 1 || n == 2)
            return 1;
        
        //overflow limit of long data type
        if (Math.Abs(n) > 92)
            throw new ArgumentOutOfRangeException("The number must be in between -92 and 92","n");

        //negoFibonacci case "negative extension part"
        if (n < 0)
        {
            n *= -1;
            long sign = (long)Math.Pow(-1, n + 1);
            return  sign * FibonacciNumber(n);
        }
       
        //recursive technique to calculate Fibonacci .. but is sooooo slow
        //return FibonacciNumber(n - 1) + FibonacciNumber(n - 2);

        //itteriative technique to calculate Fibonacci .. 
        long Fn2 = 1, Fn1 = 1, Fn = 2;
        for (int x = 2; x < n; x++)
        {
            Fn = Fn1 + Fn2;
            Fn2 = Fn1;
            Fn1 = Fn;
        }
        return Fn;

    }

    public TriangleType WhatShapeIsThis(int a, int b, int c)
    {
        
        //--------------------------------------------------------------------------------------------------------
        //  *** Triangle discussion ***
        //--------------------------------------------------------------------------------------------------------
        //  A triangle is a 3-sided polygon sometimes (but not very commonly) called the trigon. 
        //  Every triangle has three sides and three angles, some of which may be the same.
        //
        //  A scalene triangle is a triangle that has three unequal sides, such as those illustrated above.
        //
        //  An equilateral triangle is a triangle with all three sides of equal length.
        //
        //  An isosceles triangle is a triangle with (at least) two equal sides.
        //--------------------------------------------------------------------------------------------------------
        if (a <= 0 || b <= 0 || c <= 0)
            return TriangleType.Error;

        if (a == b && b == c)
            return TriangleType.Equilateral;

        if (a == b || b == c || c == a)
            return TriangleType.Isosceles;

        return TriangleType.Scalene;
    }

}


/*
    //--------------------------------------------------------------------------------------------------------
    //  None useful workarounds to solve "content type mismatch" exception
    // The correct solution is to use the right binding type which in our case the BasicHttpBinding that has SOAP1.1 and WS_Addressing V1
    //--------------------------------------------------------------------------------------------------------
        //string version = OperationContext.Current.RequestContext.RequestMessage.Version.ToString();
        //Trace.Write("WMG_MessageVersion", version);
        //WebOperationContext.Current.OutgoingResponse.ContentType = WebOperationContext.Current.IncomingRequest.ContentType;// +" , " + version;//"text/xml; charset=utf-8";

        //Old "useful" code style
        //HttpRequestMessageProperty msgProps = OperationContext.Current.IncomingMessageProperties[HttpRequestMessageProperty.Name] as HttpRequestMessageProperty;
        //string contentType = msgProps.Headers["Content-Type"] as string;
    //--------------------------------------------------------------------------------------------------------
*/