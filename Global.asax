﻿<%@ Application Language="C#" %>

<script runat="server">

    protected void Application_BeginRequest(Object Sender, EventArgs e)
    {
        if (Request.Url.PathAndQuery.Contains("RedPill"))
        {
            string uniqueid = DateTime.Now.Ticks.ToString();
            string logfile = String.Format(@"D:\Projects\yourProsoft\Readify\Docs\RequestsLog\{0}.txt", uniqueid);
            Request.SaveAs(logfile, true);
        }
    }

    
    void Application_Start(object sender, EventArgs e) 
    {
        //Load free conter when ever starting the application to prevent count per minute confliction

    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started
        //Helper.InitIP2Location(Request);
    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
    }

</script>
